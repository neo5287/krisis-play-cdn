** NOTE **

This module was added because the patch https://www.drupal.org/node/1514182

Would not apply using Drush to 2.x-dev, this was added using terminal and seems to work. Ideally would like to use a new stable release with this issue already fixed

